<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

// API route group prefix API
$router->group(['prefix' => 'api'], function () use ($router) {
  // API route group with middleware (Authorized)
    $router->group(['middleware' => 'auth'], function () use ($router) {
        // API route group with middleware (User Verified)
        $router->group(['middleware' => 'verifikasi'], function () use ($router) {
            // API route group with middleware (Admin Only)
            $router->group(['middleware' => 'role'], function () use ($router) {

            });
        });
    });

    // ==============[Endpoint Debug]==============

    $router->get('users', 'UserController@index');
    $router->post('users', 'UserController@store');
    $router->post('users/{user}', 'UserController@update');
    $router->delete('users/{user}', 'UserController@destroy');

});
