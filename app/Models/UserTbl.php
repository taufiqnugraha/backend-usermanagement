<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class UserTbl extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'tbl_users';
    protected $primaryKey = 'userid';
    protected $fillable = [
        'namalengkap', 'username', 'password', 'status'
    ];
}
