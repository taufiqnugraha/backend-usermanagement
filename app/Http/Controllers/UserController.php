<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Tymon\JWTAuth\Facades\JWTAuth; //use this library

class UserController extends Controller
{
    public function index()
    {
        return response()->json(UserTbl::select('namalengkap as name', 'username', 'status', 'password')->get(), 200);
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'status' => 'required',
            'username' => 'required',
            'password' => 'required',
        ]);

        try {
            $user = new UserTbl;
            $user->namalengkap = $request->get('name');
            $user->username = $request->get('username');
            $user->password = $request->get('password');
            $user->status = $request->get('status');
            $user->save();

            // Mail::to($email)->send(new Verification($data));
            return response()->json([
                // 'user' => $user,
                'status' => 'Success',
                'message' => 'Successfully created!',
                'data' => $user
            ], 201);

        } catch (\Exception $e) {
            return response()->json([
                'status' => 'Error',
                'message' => 'Failed register account!', 'error'=> $e->getMessage()], 409);
        }
    }

    public function update($user, Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'status' => 'required',
            'username' => 'required',
            'password' => 'required',
        ]);

        try {
            $user = UserTbl::find($user);
            $user->namalengkap = $request->name;
            $user->username = $request->username;
            $user->password = $request->password;
            $user->status = $request->status;
            $user->save();
            $message = 'Data User updated successfully';
            $status = 'Success';
            $http_code = 200;
            return response()->json([
                'status' => $status,
                'message' => $message,
                'data' => $user], $http_code);
        } catch (\Throwable $th) {
            $status = 'Error';
            $message = $th->getMessage();
            $http_code = 404;
            return response()->json([
                'status' => $status,
                'message' => $message], $http_code);
        }
    }

    public function destroy($user)
    {
        try {
            UserTbl::find($user)->delete();
            $message = 'User deleted successfully';
            $status = 'Success';
            $http_code = 200;
        } catch (\Throwable $th) {
            $status = 'Error';
            $message = $th->getMessage();
            $http_code = 404;
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
        ], $http_code);
    }
}
